using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [Header("--- Carteles de Publicidad --- \n")]
    public GameObject cartel1;
    public GameObject cartel2;
    public GameObject cartel3;
    public GameObject cartel4;
    [Header("--- Enemigos --- \n")]
    public GameObject enemigos;

    GameManager Manager;
    void Start()
    {
        Manager = GameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Manager.segundos >= 10 && Manager.segundos < 19)
        {
            cartel1.SetActive(true);
        }
        if (Manager.segundos >= 20 && Manager.segundos < 29)
        {
            cartel2.SetActive(true);
        }
        if (Manager.segundos >= 30 && Manager.segundos < 39)
        {
            cartel3.SetActive(true);
        }
        if (Manager.segundos >= 40 && Manager.segundos < 49)
        {
            cartel4.SetActive(true);
        }
        if (Time.timeScale == 0 || Manager.perdio==true)
        {
            GameObject[] enemigopresente;

            enemigopresente = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject enemigo in enemigopresente)
            {
                enemigo.SetActive(false);
            }
        }

    }
}
