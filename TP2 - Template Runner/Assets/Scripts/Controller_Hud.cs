﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controller_Hud : MonoBehaviour
{
    [Header("-----Variables Generales----- \n")]
    public static Controller_Hud instance = null;
    private GameManager Manager;
    public bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public float distance = 0;
    [Header("-----Barra de Progreso----- \n")]
    public Image barraProgreso;
    public float maximo;
    public float minimo;
    public float rellenar;
    [Header("-----Combustible----- \n")]
    public Image bateriaFull;
    public Image bateria2;
    public Image bateria1;
    public Image bateriaEmpty;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

    }
    void Start()
    {
        Manager = GameManager.instance;
        gameOver = false;
        distance = 0;
        distanceText.text = "Distancia Recorrida: " + distance.ToString("0");
        gameOverText.gameObject.SetActive(false);
        
    }

    void Update()
    {
        SinGas();
        Rellenar();
        if (gameOver==true)
        {
            Manager.perdio = true;
            StartCoroutine(GameOverHUD2());
            
        }
        if (gameOver==false && SceneManager.GetActiveScene()!= SceneManager.GetSceneByName("MENU"))
        {
            gameOverText.gameObject.SetActive(false);
            distance += Time.deltaTime;
            distanceText.text = "Distancia Recorrida: " + distance.ToString("0") + " KM";
        }
    }
    private void GameOverHUD()
    {
        Time.timeScale = 0;
        gameOverText.text = "Game Over \n Distancia total: " + distance.ToString("0") + " Kilometros." + " \n Tiempo total: " + Manager.segundos.ToString("0") + " Segundos.";
        gameOverText.gameObject.SetActive(true);
        Manager.GuardarDatos();
        
    }

    private void Rellenar()
    {
        minimo -= 1 * Time.deltaTime;
        rellenar = (float)minimo / (float)maximo;
        barraProgreso.fillAmount = rellenar;
    }

    private void SinGas()
    {
        if (minimo <= 0)
        {
            gameOver = true;
        }
    }
    
    IEnumerator GameOverHUD2()
    {
        //Time.timeScale = 0;
        gameOverText.text = "Game Over \n Distancia total: " + distance.ToString("0") + " Kilometros." + " \n Tiempo total: " + Manager.segundos.ToString("0") + " Segundos.";
        gameOverText.gameObject.SetActive(true);
        Manager.GuardarDatos();

        GameObject[] enemigopresente;

        enemigopresente = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject enemigo in enemigopresente)
        {
            enemigo.SetActive(false);
        }

        GameObject[] gas;

        gas = GameObject.FindGameObjectsWithTag("Gas");

        foreach (GameObject objeto in gas)
        {
            objeto.SetActive(false);
        }

        GameObject instantiator = GameObject.Find("Instantiator");
        instantiator.SetActive(false);

        GameObject[] parallax = GameObject.FindGameObjectsWithTag("Parallax");
        foreach (GameObject parte in parallax)
        {
            Parallax script = parte.GetComponent<Parallax>();
            script.enabled = false;
        }

        GameObject[] puente = GameObject.FindGameObjectsWithTag("Puente");
        foreach (GameObject parte in puente)
        {
            ParallaxPuente script = parte.GetComponent<ParallaxPuente>();
            script.enabled = false;
        }
        

        yield return new WaitForSeconds(2f);
        gameOver = false;
        distance = 0;
        minimo = 100;
        Manager.segundos = 0;
        SceneManager.LoadScene("MENU");
        yield break;

    }
    


}
