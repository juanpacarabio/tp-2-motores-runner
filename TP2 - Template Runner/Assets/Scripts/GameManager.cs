using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public bool gano;
    public bool perdio;
   

    [Header("------Player Data------ \n")]
    public string nombre;
    public float segundos;

    private Controller_Hud HUD;

    [Header("------Leaderboard------ \n")]
    public string top1nombre = "Hideo K.";
    public float top1 = 10;
    public string top2nombre = "John R.";
    public float top2 = 7;
    public string top3nombre = "Gtxy Co.";
    public float top3 = 5;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }
    void Start()
    {
        HUD = Controller_Hud.instance;
        perdio = false;
        gano = false;
        CargarDatos();
    }

    // Update is called once per frame
    void Update()
    {
       
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("MENU")&& perdio==false)
        {
            segundos += 1 * Time.deltaTime;
        }
    
    }

    public void GuardarDatos()
    {

        if (segundos > top1)
        {
            top3 = top2;
            top3nombre = top2nombre;
            top2 = top1;
            top2nombre = top1nombre;
            top1 = segundos;
            top1nombre = nombre;
        }else if (segundos < top1)
        {
            if (segundos > top2)
            {
                top3 = top2;
                top3nombre = top2nombre;
                top2 = segundos;
                top2nombre = nombre;
            }else if (segundos < top2)
            {
                if (segundos > top3)
                {
                    top3 = segundos;
                    top3nombre = nombre;
                }
            }
        }

        SaveSystem.SavePlayer(this);
    }

    public void CargarDatos()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        top1nombre = data.nombreTop1;
        top1 = data.distanciaTop1;
        top2nombre = data.nombreTop2;
        top2 = data.distanciaTop2;
        top3nombre = data.nombreTop3;
        top3 = data.distanciaTop3;
    }

    

}
