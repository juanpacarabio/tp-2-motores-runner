﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    private Controller_Hud HUD;
    public float combustible;
    GameManager manager;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        HUD = Controller_Hud.instance;
        manager = GameManager.instance;
    }

    private void Update()
    {
        Bateria();
        if (floored)
        {
            if (combustible < 5)
            {
                combustible = combustible + (Time.deltaTime);
            }

        }
    }

    public void Jump()
    {
        if (combustible > 0)
        {
            if (Input.GetKey(KeyCode.W))
            {
                Debug.Log("Salto");
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                combustible = combustible - Time.deltaTime;
            
            }
        }
        
    }

    public void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -(jumpForce / 2), 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            HUD.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Gas"))
        {
            Destroy(other.gameObject);
            HUD.minimo += 10;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }

    private void Bateria()
    {
        if (combustible >= 5)
        {
            HUD.bateriaFull.gameObject.SetActive(true);
            HUD.bateria2.gameObject.SetActive(false);
            HUD.bateria1.gameObject.SetActive(false);
            HUD.bateriaEmpty.gameObject.SetActive(false);

        }
        else if(combustible <4 && combustible >= 2.5)
        {
            HUD.bateriaFull.gameObject.SetActive(false);
            HUD.bateria2.gameObject.SetActive(true);
            HUD.bateria1.gameObject.SetActive(false);
            HUD.bateriaEmpty.gameObject.SetActive(false);
        }
        else if (combustible < 2.5 && combustible >= 0)
        {
            HUD.bateriaFull.gameObject.SetActive(false);
            HUD.bateria2.gameObject.SetActive(false);
            HUD.bateria1.gameObject.SetActive(true);
            HUD.bateriaEmpty.gameObject.SetActive(false);
        }
        else if (combustible <= 0)
        {
            HUD.bateriaFull.gameObject.SetActive(false);
            HUD.bateria2.gameObject.SetActive(false);
            HUD.bateria1.gameObject.SetActive(false);
            HUD.bateriaEmpty.gameObject.SetActive(true);
        }


    }

    public void MenuReturn()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SceneManager.LoadScene("MENU");
            HUD.gameOver = false;
            HUD.distance = 0;
            HUD.minimo = 100;
            manager.segundos = 0;
        }
    }


}
