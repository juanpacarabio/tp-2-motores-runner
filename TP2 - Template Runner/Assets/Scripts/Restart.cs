﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    GameObject enemy;
    public Controller_Hud HUD;
    public GameManager manager;

    private void Start()
    {
        enemy = GameObject.Find("Instantiator");
        HUD = Controller_Hud.instance;
        manager = GameManager.instance;
    }
    private void Update()
    {
        HUD = Controller_Hud.instance;
    }
    public void StartOver()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            manager.segundos = 0;
            HUD.gameOver = false;
            HUD.distance = 0;
            Time.timeScale = 1;
            HUD.minimo = 100;

            GameObject[] enemigopresente;

            enemigopresente = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject enemigo in enemigopresente)
            {
                enemigo.SetActive(false);
            }

            GameObject[] gas;

            gas = GameObject.FindGameObjectsWithTag("Gas");

            foreach (GameObject objeto in gas)
            {
                objeto.SetActive(false);
            }

        }
    }
}
 