using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [System.Serializable]
public class PlayerData
{
    public float distanciaTop1;
    public float distanciaTop2;
    public float distanciaTop3;

    public string nombreTop1;
    public string nombreTop2;
    public string nombreTop3;

    public PlayerData (GameManager manager)
    {

        distanciaTop1 = manager.top1;
        distanciaTop2 = manager.top2;
        distanciaTop3 = manager.top3;

        nombreTop1 = manager.top1nombre;
        nombreTop2 = manager.top2nombre;
        nombreTop3 = manager.top3nombre;

    }
}
