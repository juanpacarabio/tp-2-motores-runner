using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolEnemigo1 : MonoBehaviour
{
    public static PoolEnemigo1 SharedInstance;
    public List<GameObject> objetos;
    public GameObject objeto;
    public int cantidad;

    void Awake()
    {
        SharedInstance = this;
    }

    void Start()
    {
        objetos = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < cantidad; i++)
        {
            tmp = Instantiate(objeto);
            tmp.SetActive(false);
            objetos.Add(tmp);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < cantidad; i++)
        {
            if (!objetos[i].activeInHierarchy)
            {
                return objetos[i];
            }
        }
        return null;
    }

}
