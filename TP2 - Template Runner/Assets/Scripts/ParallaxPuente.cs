﻿using UnityEngine;

public class ParallaxPuente : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    public float distanciaRepeticion;

    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        if (Time.timeScale == 1)
        {
            if (this.gameObject.CompareTag("Puente"))
            {
                transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
                if (transform.localPosition.x < -20)
                {
                    transform.localPosition = new Vector3(distanciaRepeticion, transform.localPosition.y, transform.localPosition.z);
                }
            }
            if (this.gameObject.CompareTag("AD"))
            {
                transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
                if (transform.localPosition.x < -30)
                {
                    Destroy(gameObject);
                }
            }

        }
        
    }
}
