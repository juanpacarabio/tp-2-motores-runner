using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public GameObject menuPrincipal;
    public GameObject creditos;
    public GameObject teclas;
    public GameObject leaderboards;
    public GameObject jugar;
    public GameObject nombre;
    public InputField IF;
    public TMPro.TMP_InputField txtNombre;
    GameManager Manager;
    Controller_Hud HUD;

    private void Start()
    {
        Manager = GameManager.instance;
        HUD = Controller_Hud.instance;
    }


    public void Jugar()
    {
        menuPrincipal.SetActive(false);
        jugar.SetActive(true);
        nombre.SetActive(false);
        teclas.SetActive(false);
        leaderboards.SetActive(false);
        creditos.SetActive(false);
    }

    public void Creditos()
    {
        menuPrincipal.SetActive(false);
        creditos.SetActive(true);
        teclas.SetActive(false);
        nombre.SetActive(false);
        leaderboards.SetActive(false);
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void MenuPrincipa()
    {
        jugar.SetActive(false);
        menuPrincipal.SetActive(true);
        teclas.SetActive(false);
        leaderboards.SetActive(false);
        creditos.SetActive(false);
    }

    public void Leader()
    {
        jugar.SetActive(false);
        leaderboards.SetActive(true);
        creditos.SetActive(false);
        teclas.SetActive(false);
        menuPrincipal.SetActive(false);
    }

    public void Teclas ()
    {
        jugar.SetActive(false);
        teclas.SetActive(true);
        menuPrincipal.SetActive(false);
        creditos.SetActive(false);

    }

    public void Volver1()
    {
        jugar.SetActive(false);
        creditos.SetActive(false);
        nombre.SetActive(false);
        menuPrincipal.SetActive(true);

    }

    public void Volver2()
    {
        teclas.SetActive(false);
        leaderboards.SetActive(false);
        nombre.SetActive(false);
        jugar.SetActive(true);
    }

    public void Nombre()
    {
        jugar.SetActive(false);
        nombre.SetActive(true);
    }

    public void IniciarPartida()
    {
        Manager.perdio = false;
        Manager.nombre = txtNombre.text;
        SceneManager.LoadScene("JUANTEST2");
    }





}
