using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeadeBoard : MonoBehaviour
{
    public TMPro.TMP_Text nombre1;
    public TMPro.TMP_Text nombre2;
    public TMPro.TMP_Text nombre3;
    public TMPro.TMP_Text score1;
    public TMPro.TMP_Text score2;
    public TMPro.TMP_Text score3;
    GameManager Manager;

    void Start()
    {
        Manager = GameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Manager.top1nombre == "" && Manager.top2nombre == "" && Manager.top2nombre == "")
        {
            Manager.top1nombre = "Hideo K.";
            Manager.top2nombre = "John R.";
            Manager.top3nombre = "Gotoxy Co.";
        }
        if(Manager.top1nombre == "Hideo K.")
        {
            Manager.top1 = 12;
        }
        if(Manager.top2nombre == "John R.")
        {
            Manager.top2 = 9;
        }
        if(Manager.top3nombre == "Gotoxy Co.")
        {
            Manager.top3 = 5;
        }

        nombre1.text = Manager.top1nombre;
        nombre2.text = Manager.top2nombre;
        nombre3.text = Manager.top3nombre;
        score1.text = Manager.top1.ToString("0");
        score2.text = Manager.top2.ToString("0");
        score3.text = Manager.top3.ToString("0");

    }





}
