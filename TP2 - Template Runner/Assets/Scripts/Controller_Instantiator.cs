﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> instantiatePos;
    public float respawningTimer;
    private float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 0.5f;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(0.5f, 5f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            int elementoInstanciado = Random.Range(0, 4);
            if (elementoInstanciado == 0) //enemigo 1
            {
                GameObject enemigo = PoolEnemigo1.SharedInstance.GetPooledObject();
                if (enemigo != null)
                {
                    int posicion = Random.Range(0, 3);
                    enemigo.transform.position = instantiatePos[posicion].transform.position;
                    enemigo.transform.rotation = instantiatePos[posicion].transform.rotation;
                    enemigo.SetActive(true);
                }
            }
            if (elementoInstanciado == 1) //enemigo 2
            {
                GameObject enemigo = PoolEnemigo2.SharedInstance.GetPooledObject();
                if (enemigo != null)
                {
                    int posicion = Random.Range(0, 3);
                    enemigo.transform.position = instantiatePos[posicion].transform.position;
                    enemigo.transform.rotation = instantiatePos[posicion].transform.rotation;
                    enemigo.SetActive(true);
                }
            }
            if (elementoInstanciado == 2) //enemigo 3
            {
                GameObject enemigo = PoolEnemigo3.SharedInstance.GetPooledObject();
                if (enemigo != null)
                {
                    int posicion = Random.Range(0, 3);
                    enemigo.transform.position = instantiatePos[posicion].transform.position;
                    enemigo.transform.rotation = instantiatePos[posicion].transform.rotation;
                    enemigo.SetActive(true);
                }
            }
            if (elementoInstanciado >= 3) //gas
            {
                GameObject enemigo = PoolGas.SharedInstance.GetPooledObject();
                if (enemigo != null)
                {
                    int posicion = Random.Range(0, 3);
                    enemigo.transform.position = instantiatePos[posicion].transform.position;
                    enemigo.transform.rotation = instantiatePos[posicion].transform.rotation;
                    enemigo.SetActive(true);
                }
            }



            //Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }
}
