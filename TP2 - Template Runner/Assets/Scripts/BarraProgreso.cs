using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraProgreso : MonoBehaviour
{
    public Image barra;
    public float maximo;
    public float actual;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rellenar();
    }

    private void Rellenar()
    {
        float rellenar = (float)actual / (float)maximo;
        barra.fillAmount = rellenar;
    }

}
