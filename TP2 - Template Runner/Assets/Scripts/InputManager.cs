using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    GameObject player;
    Controller_Player ControlJugador;
    GameManager manager;
    Restart restart;
    void Start()
    {
        manager = GameManager.instance;
        player = GameObject.Find("Player");
        ControlJugador = player.GetComponent<Controller_Player>();
        restart = manager.GetComponent<Restart>();
    }

    
    void Update()
    {
        GetInput(); 
    }

    private void GetInput()
    {
        ControlJugador.Jump();
        ControlJugador.Duck();
        restart.StartOver();
        ControlJugador.MenuReturn();
    }
}
